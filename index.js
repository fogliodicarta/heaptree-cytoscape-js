var cy;
var inputxt;
var nnodi=0;
var tree=new Array();
var indice;
var codaAnimazioni=new Array();
var statusArrAnim=new Array();
var minSelected=false;

function datiArrivati(data) {
  tree=new Array();
  var iframe = document.getElementById('iframe');
  iframe.src = iframe.src;
  for (var i = 0; i < data.length; i++) {
    tree[parseInt(data[i].indice,10)]=parseInt(data[i].peso,10);
  }
  nnodi=tree.length;
  zoomAndDisegna();
}
function disegnatutto(arr){
  cy.elements().remove()
  for (let i = 0; i < arr.length; i++) {
    cy.add({
           group: 'nodes',
           data: { id:i,peso:""+arr[i]},
           position: { x: 200, y: 200 }
        });
  }
  edgeadder(0,arr);
  var layout = cy.layout({
    name: 'breadthfirst',
    directed: true,
    grid: true,
    roots: "#0"
  });
  layout.run();
}
function edgeadder(j,arr){
  if (left(j)<arr.length) {
    var ledge={
           group: 'edges',
           data: {id:"e"+j+""+left(j),source: j, target: left(j)},
           position: { x: 200, y: 200 },
           style:{
             'line-color':'#fff'
           }
        }
    cy.add(ledge);
    cy.$id("e"+j+""+left(j)).animate({
     style: { lineColor: 'black' }
      },
      {
       duration: 1500
     }
    );
     edgeadder(left(j),arr);
  }
  if (right(j)<arr.length) {
    var redge={
           group: 'edges',
           data: {id:"e"+j+""+right(j),source: j, target: right(j)},
           position: { x: 200, y: 200 },
           style:{
             'line-color':'#fff'
           }
        }
    cy.add(redge);
    cy.$id("e"+j+""+right(j)).animate({
     style: { lineColor: 'black' }
      },
      {
       duration: 1500
     }
    );
     edgeadder(right(j),arr);
  }
}
function animazioneScambio(ifigliomagg,ipadre){
 var figlio=cy.getElementById(''+ifigliomagg);
 var padre=cy.getElementById(''+ipadre);
 padre.connectedEdges().animate({
  style: { lineColor: 'white' }
   },
   {
    duration: 500
  }
 );
 figlio.connectedEdges().animate({
  style: { lineColor: 'white' }
   },
   {
    duration: 500
  }
 );
 if(figlio.position()!==undefined){
 var posInizPadre={x:padre.position().x,y:padre.position().y};
 var posInizFiglio={x:figlio.position().x,y:figlio.position().y}
 figlio.animate({
  position: posInizPadre,//RICORDA CHE HAI USATO UNA VARIABILE E JSON PARSE STRINGIFY SE NO SI INCONTRANO
  style: { backgroundColor: 'green' }
   }, {
    duration: 2000
   });
  padre.animate(
     {
       position: posInizFiglio,
       style: { backgroundColor: 'red' }
      },
      {
       duration: 2000,
       complete:function(){
        disegnatutto(statusArrAnim.shift());
    }
      }
  );
}
}
function swap(x,y) {
    let supporto = tree[x];
    tree[x] = tree[y];
    tree[y] = supporto;
}
function parent(i){
  return Math.trunc((i-1)/2);
}
function left(i){
  return (2 * i) + 1;
}
function right(i) {
  return (2 * i) + 2;
}
function isLeaf(i) {
  return left(i) >= nnodi;
}
function insertMx(val) {
  tree.push(val);
  nnodi=tree.length;
}
function buildMinHeap() {
    statusArrAnim=new Array();
    codaAnimazioni=new Array();
    for (let i = tree.length; i >= 0; i--) {
        minHeapify(i);
    }
}
function minHeapify(i) {
    let l = left(i);
    let r = right(i);
    let smallest = i;
    if (l < nnodi && tree[l] < tree[smallest]) {
        smallest = l;
    }
    if (r < nnodi && tree[r] < tree[smallest]) {
        smallest = r;
    }
    if (smallest != i) {
        swap(smallest, i);
        codaAnimazioni.push({f:smallest,p:i});
        statusArrAnim.push(Array.from(tree));
        minHeapify(smallest);
    }
}
function buildMaxHeap() {
    statusArrAnim=new Array();
    codaAnimazioni=new Array();
    for (let i = tree.length; i >= 0; i--) {
        maxHeapify(i);
    }
}
function maxHeapify(i) {
    let l = left(i);
    let r = right(i);
    let largest = i;
    if (l < nnodi && tree[l] > tree[largest]) {
        largest = l;
    }
    if (r < nnodi && tree[r] > tree[largest]) {
        largest = r;
    }
    if (largest != i) {
        swap(largest, i);
        codaAnimazioni.push({f:largest,p:i});
        statusArrAnim.push(Array.from(tree));
        maxHeapify(largest);
    }
}
function heapSort(){
  if (!minSelected) {
    buildMinHeap();
    for (let i = nnodi-1; i >=0; i--) {
      swap(0,i);
      nnodi--;
      minHeapify(0);
    }
  }else{
    buildMaxHeap();
    for (let i = nnodi-1; i >=0; i--) {
      swap(0,i);
      nnodi--;
      maxHeapify(0);
    }
  }
  nnodi=tree.length
  disegnatutto(tree);
}
window.onload= function(){
  document.getElementById('leva').checked=false;
  inputxt=document.getElementById('name');
  document.getElementById('next').addEventListener("click",function(){
    var currentAnim=codaAnimazioni.shift();
    if(currentAnim!==undefined){
      animazioneScambio(currentAnim.f,currentAnim.p);
    }
   });
  document.getElementById('addNode').addEventListener("click",function(){
    insertMx(parseInt(inputxt.value,10))
    zoomAndDisegna();
  });
  document.getElementById('heapizza').addEventListener("click",function(){
    if (minSelected) {
      buildMinHeap();
    }else{
     buildMaxHeap();
    }
  });
  document.getElementById('importa').addEventListener("click",function(){
    Sheetsu.read("https://sheetsu.com/apis/v1.0su/b6bc8ea7174b", {}, datiArrivati);
  });
  document.getElementById('rm').addEventListener("click",function(){
    var positions=new Array();
    for (var i = 0; i < tree.length; i++) {
      positions[i]={};
      let pos =cy.$id(''+i).position();
      positions[i].x=pos.x;
      positions[i].y=pos.y;
    }
    var rimosso=cy.$id(inputxt.value);
    var figli=rimosso
    cy.remove(rimosso);
    let lin=left(parseInt(inputxt.value,10));
    let rin=right(parseInt(inputxt.value,10));
    if(minSelected){
      let smaller=tree[lin]<tree[rin]?lin:rin;
      hoistNode(positions,smaller);
      tree.splice(parseInt(inputxt.value,10),1);
      buildMinHeap();
    }else{
      let bigger=tree[lin]>=tree[rin]?lin:rin;
      hoistNode(positions,bigger);
      tree.splice(parseInt(inputxt.value,10),1);
      buildMaxHeap();
    }
    nnodi=tree.length;
  });
  function hoistNode(positions,ind){//animazione per sollevare il nodo
    cy.$id(""+ind).animate(
      {
        position:positions[parent(ind)]
      },
      {
        duration:1000,
        complete:function(){
          var positions2=new Array();
          for (var i = ind; i < tree.length; i++) {
            positions2[i]={};
            let pos =cy.$id(''+i).position();
            positions2[i].x=pos.x;
            positions2[i].y=pos.y;
          }
          let lin=left(ind);
          let rin=right(ind);
          let bigger=tree[lin]>=tree[rin]?lin:rin;
          hoistNode(positions,bigger);
          if (isLeaf(ind)){
            cy.delay(500);
            zoomAndDisegna();
          }
        }
      }
    );
  }
  document.getElementById('sort').addEventListener("click",function(){
    heapSort();
  });
  document.getElementById('leva').addEventListener("change",function(){
     var heapizza=document.getElementById('heapizza');
     var next = document.getElementById('next');
     var adders=document.getElementsByClassName('adder');
     minSelected=this.checked;
     if (minSelected) {
       heapizza.innerHTML="Min Heapify";
       for (let i = 0; i < adders.length; i++) {
         adders[i].style.backgroundColor="#0f9d58";
       };
       cy.style().resetToDefault().update();
       zoomAndDisegna();
       setTimeout(function(){
         cy.style( [
              {
                selector: 'node',
                style: {
                  'label': 'data(peso)',
                  'text-valign':'center',
                  'background-color':'#0f9d58',
                  'color':'white',
                  'font-size' : '20px',
                  'font-weight' : 'bold'
                }
              },
              {
                selector:'edge',
                style:{
                  'curve-style': 'bezier',
                  'line-color':'black'
                }
              }
            ]).update();
       },500);
     }else{
       heapizza.innerHTML="Max Heapify";
       for (let i = 0; i < adders.length; i++) {
         adders[i].style.backgroundColor="#ff6600";
       }
       cy.style().resetToDefault().update();
       zoomAndDisegna();
       setTimeout(function(){
         cy.style( [
              {
                selector: 'node',
                style: {
                  'label': 'data(peso)',
                  'text-valign':'center',
                  'background-color':'#ff6600',
                  'color':'white',
                  'font-size' : '20px',
                  'font-weight' : 'bold'
                }
              },
              {
                selector:'edge',
                style:{
                  'curve-style': 'bezier',
                  'line-color':'black'
                }
              }
            ]).update();
       },500);
     }
  });

   cy = cytoscape({
    container : document.getElementById('cy'),
  });
  cy.style( [
     {
       selector: 'node',
       style: {
         'label': 'data(peso)',
         'text-valign':'center',
         'background-color':'#ff6600',//alternative #1E90FF
         'color':'white',   //alternative #FFD700
         'font-size' : '20px',
         'font-weight' : 'bold'
       }
     },
     {
       selector:'edge',
       style:{
         'curve-style': 'bezier',
         'line-color':'black'
       }
     }
   ]).update()
}
function zoomAndDisegna(){
  var z=cy.zoom();
  cy.animate(
    {
      zoom:{
        position:{x:500,y:500},
        level:5
      }
    },
    {
      complete:function(){
        cy.animate(
          {
            zoom:{
              position:{x:500,y:500},
              level:z
            }
          },{  complete:function(){
              disegnatutto(tree);
          }
        }
        );
      }
    }
  );
}
